# **Kanvas**

O Kanvas é um sistema de autenticação e permissões para criação e inscrição em cursos, subissão de atividades e atribuição de notas.

### **Instalando e rodando o projeto**

Baixando o projeto:

```sh
git clone https://gitlab.com/acuglar/kanvas.git
```

Depois de baixar, é necessário entrar na pasta, criar um ambiente virtual e ativa-lo:

```sh
cd kanvas  # Entrar na pasta onde o projeto foi baixado
python3 -m venv venv  # Criar um ambiente virtual
source venv/bin/activate  # Ativar ambiente virtual
```

Instalando dependências:

```sh
pip install -r requirements.txt
```

Rodando as migrations para que o banco de dados e as tabelas sejam criadas:

```sh
./manage.py migrate
```

Rodando o projeto kanvas localmente:

```sh
./manage.py runserver
# O sistema estará rodando em http://127.0.0.1:8000/
```

Rodando os testes:
```sh
python manage.py test -v 2 &> report.txt
```
### **Utilização**

Para utilizar este sistema, é necessário utilizar um API Client, como o [Insomnia](https://insomnia.rest/download)

Esta plataforma tem 3 tipos de usuário:

- Estudante - possui ambos os campos `is_staff` e `is_superuser` com o valor False
- Facilitador - possui o campo `is_staff` == True e `is_superuser` == False
- Instrutor - possui os campos `is_staff` e `is_superuser` com o valor True

## **Rotas**

### **Criação de usuários:**

**POST /api/accounts/**  
(cria qualquer tipo de usuário)

```json
// REQUEST
{
  "username": "student",
  "password": "1234",
  "is_superuser": false,
  "is_staff": false
}

// RESPONSE STATUS -> HTTP 201 (Created)
{
  "id": 1,
  "username": "student",
  "is_superuser": false,
  "is_staff": false
}

// Caso haja a tentativa de criação de um usuário que já cadastrado o sistema deverá responder:
// RESPONSE STATUS ->  HTTP 409 (Conflict).
```

---

### **Autenticação:**  
A API funcionará com autenticação baseada em token.

Esse token servirá para identificar o usuário em cada request.  
Na grande maioria dos endpoints seguintes, será necessário colocar essa informação nos Headers na API CLient no formato: Authorization: Token <token gerado pela rota login>.

**POST /api/login/**

```json
// REQUEST
{
  "username": "student",
  "password": "1234"
}

// RESPONSE STATUS -> HTTP 200 (Ok)
{
  "token": "dfd384673e9127213de6116ca33257ce4aa203cf"
}

// Caso haja a tentativa de login de uma conta que ainda não tenha sido criada:
// RESPONSE STATUS -> HTTP 401 (Unauthorized).
```

---

### **Permissões:**

Todos os endpoints que exigirem a utilização de um token de acesso deverão responder da seguinte maneira:

- Token inválido

```json
// REQUEST
// Header -> Authorization: Token <token-inválido>

// RESPONSE STATUS -> HTTP 401 (Unauthorized)
{
  "detail": "Invalid token."
}
```

- Token válido, porém não atenda aos requisitos mínimos de permissão do tipo de usuário

```json
// REQUEST
// Header -> Authorization: Token <token-não-atende-aos-requisitos>

// RESPONSE STATUS -> HTTP 403
{
  "detail": "You do not have permission to perform this action."
}
```

---

### **Cursos:**

**POST /api/courses/**

Rota para criação de um curso

```json
// REQUEST
// Header -> Authorization: Token <token-do-instrutor>
{
  "name": "Node"
}

// RESPONSE STATUS -> HTTP 201 (Created)
{
  "id": 1,
  "name": "Node",
  "users": []
}

//caso haja a tentativa de criação de um curso já existente, o sistema não deverá criar um novo curso, apenas retornar o curso já existente.
```

**PUT /api/courses/\<int:course_id>/registrations/**  
(atualizando a lista de estudantes matriculados em um curso)

Deverá ser informada uma lista de id's de estudantes a serem matriculados. A lista de id's fornecida substitui a lista atual indicando todos os estudantes matriculados no curso especificado.

Somente usuários do tipo estudante podem ser matriculados no curso.

```json
// REQUEST
// Header -> Authorization: Token <token-do-instrutor>
{
  "user_ids": [3, 4, 5]
}

// RESPONSE STATUS -> HTTP 200 (Ok)
{
  "id": 1,
  "name": "Node",
  "users": [
    {
    "id": 3,
    "username": "student1"
    },
    {
    "id": 4,
    "username": "student2"
    },
    {
    "id": 5,
    "username": "student3"
    }
  ]
}

// Caso não seja informada uma lista:
// RESPONSE STATUS -> HTTP 400 (Bad request)

// Caso não seja estudante:
// RESPONSE STATUS -> HTTP 400 (Bad request)
{
  "errors": "Only students can be enrolled in the course."
}

// Caso seja informado um course_id inválido:
// RESPONSE STATUS -> HTTP 404 (Not found)
{
  "errors": "invalid course_id"
}

// Caso seja informado um user_id inválido:
// RESPONSE STATUS -> HTTP 404 (Not found)
{
  "errors": "invalid user_id list"
}
```

**GET /api/courses/**  
(obtendo a lista de cursos e alunos)

Pode ser acessado por qualquer client mesmo sem autenticação.

```json
// RESPONSE STATUS -> HTTP 200 (Ok)
[
  {
    "id": 1,
    "name": "Node",
    "users": [
      {
        "id": 3,
        "username": "student1"
      }
    ]
  },
  {
    "id": 2,
    "name": "Django",
    "users": []
  },
  {
    "id": 3,
    "name": "React",
    "users": []
  }
]
```

**GET /api/courses/<int:course_id>/**  
(filtrando a lista de cursos, fornecendo um course_id opcional)

Pode ser acessado por qualquer client mesmo sem autenticação.

```json
// RESPONSE STATUS -> HTTP 200 (Ok)
[
  {
    "id": 1,
    "name": "Node",
    "users": [
      {
        "id": 3,
        "username": "student1"
      }
    ]
  }
]

// Caso seja informado um course_id inválido:
// RESPONSE STATUS -> HTTP 404 (Not found)
{
  "errors": "invalid course_id"
}
```

**DELETE /api/courses/<int:course_id>/**

```json
// REQUEST
// Header -> Authorization: Token <token-do-instrutor>

// RESPONSE STATUS -> HTTP 204 (No Content)

// Caso seja informado um course_id inválido:
// RESPONSE STATUS -> HTTP 404 (Not found)
```

---

### **Atividades e Submissões**

**Activity** representa uma atividade cadastrada no sistema pelos facilitadores ou instrutores para que os alunos possam fazer suas submissões.

**Submission** representa uma submissão de uma atividade feita por um aluno.

**POST /api/activities/**  
(criando uma atividade)

```json
// REQUEST
// Header -> Authorization: Token <token-do-facilitador ou token-do-instrutor>
{
  "title": "Kenzie Pet",
  "points": 10
}

// RESPONSE STATUS -> HTTP 201 (Created)
{
  "id": 1,
  "title": "Kenzie Pet",
  "points": 10,
  "submissions": []
}

// Caso haja a tentativa de criação de uma atividade com o mesmo título retorna a atividade já existente.
```

**GET /api/activities/**  
(listando atividades)

```json
// REQUEST
// Header -> Authorization: Token <token-do-instrutor ou token-do-facilitador>

// RESPONSE STATUS -> HTTP 200 (Ok)
[
  {
    "id": 1,
    "title": "Kenzie Pet",
    "points": 10,
    "submissions": [
      {
        "id": 1,
        "grade": 10,
        "repo": "http://gitlab.com/kenzie_pet",
        "user_id": 3,
        "activity_id": 1
      }
    ]
  },
  {
    "id": 2,
    "title": "Kanvas",
    "points": 10,
    "submissions": [
      {
        "id": 2,
        "grade": 8,
        "repo": "http://gitlab.com/kanvas",
        "user_id": 4,
        "activity_id": 2
      }
    ]
  },
  {
    "id": 3,
    "title": "KMDb",
    "points": 9,
    "submissions": [
      {
        "id": 3,
        "grade": 4,
        "repo": "http://gitlab.com/kmdb",
        "user_id": 5,
        "activity_id": 3
      }
    ]
  }
]
```

**POST /api/activities/<int:activity_id>/submissions/**  
(fazendo submissão de uma atividade)

```json
// REQUEST
// Header -> Authorization: Token <token-do-estudante>
{
  "grade": 10, // Esse campo é opcional
  "repo": "http://gitlab.com/kenzie_pet"
}

// RESPONSE STATUS -> HTTP 201 (Ok)
{
  "id": 7,
  "grade": null,  // alunos não podem dar nota a si mesmos
  "repo": "http://gitlab.com/kenzie_pet",
  "user_id": 3,
  "activity_id": 1
}
```

**PUT /api/submissions/<int:submission_id>/**  
(editando a nota de uma submissão)

```json
// REQUEST
// Header -> Authorization: Token <token-do-facilitador ou token-do-instrutor>
{
  "grade": 10
}

// RESPONSE STATUS -> HTTP 200 (Ok)
{
  "id": 3,
  "grade": 10,
  "repo": "http://gitlab.com/kenzie_pet",
  "user_id": 3,
  "activity_id": 1
}
```

**GET /api/submissions/**  
(listando submissões)

Caso seja informado um token de estudante o sistema deverá retornar apenas as submissões daquele estudante:

```json
//REQUEST
//Header -> Authorization: Token <token-do-estudante>

// RESPONSE STATUS -> HTTP 200 (Ok)
[
  {
    "id": 2,
    "grade": 8,
    "repo": "http://gitlab.com/kanvas",
    "user_id": 4,
    "activity_id": 2
  },
  {
    "id": 5,
    "grade": null,
    "repo": "http://gitlab.com/kmdb2",
    "user_id": 4,
    "activity_id": 1
  }
]
```

caso seja informado um token de facilitador ou token de instrutor, a aplicação responderá com todas as submissões de todos os estudantes.

```json
//REQUEST
//Header -> Authorization: Token <token-do-facilitador ou token-do-instrutor>

// RESPONSE STATUS -> HTTP 200 (Ok)
[
  {
    "id": 1,
    "grade": 10,
    "repo": "http://gitlab.com/kenzie_pet",
    "user_id": 3,
    "activity_id": 1
  },
  {
    "id": 2,
    "grade": 8,
    "repo": "http://gitlab.com/kanvas",
    "user_id": 4,
    "activity_id": 2
  },
  {
    "id": 3,
    "grade": 4,
    "repo": "http://gitlab.com/kmdb",
    "user_id": 5,
    "activity_id": 3
  },
  {
    "id": 4,
    "grade": null,
    "repo": "http://gitlab.com/kmdb2",
    "user_id": 5,
    "activity_id": 3
  }
]
```

## **Tecnologias utilizadas** 📱

- Django
- Django Rest Framework
- SQLite

