from rest_framework.permissions import BasePermission


class IsInstructor(BasePermission):
    def has_permission(self, request, view):
        user = request.user

        return user.is_superuser and user.is_staff


class IsFacilitator(BasePermission):
    def has_permission(self, request, view):
        user = request.user

        return not user.is_superuser and user.is_staff


class IsStudent(BasePermission):
    def has_permission(self, request, view):
        user = request.user

        return not user.is_superuser and not user.is_staff


