from django.shortcuts import render
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.db import IntegrityError
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from accounts.serializers import UserSerializer, LoginSerializer


class UserView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  
        is_staff = serializer.data['is_staff']
        is_superuser = serializer.data['is_superuser']

        if not is_staff and is_superuser:
            return Response(
                {'errors': 'user must be a student, facilitator or instructor'}, 
                status=status.HTTP_400_BAD_REQUEST
                )

        try: 
            user = User.objects.create_user(**serializer.validated_data)
        except IntegrityError:
            return Response({'errors': 'user already exists'}, status=status.HTTP_409_CONFLICT)
        
        serializer = UserSerializer(user)
        
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class LoginView(APIView):
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        user = authenticate(
            username=serializer.validated_data["username"],
            password=serializer.validated_data["password"]
            )  # user = authenticate(**serializer.validated_data)

        print(user)
      
        if user:
            token, _ = Token.objects.get_or_create(user=user)
            return Response({'token': token.key}, status=status.HTTP_200_OK)
        else:
            return Response(
                {'errors': 'user or password you entered is incorrect'}, 
                status=status.HTTP_401_UNAUTHORIZED
                )


