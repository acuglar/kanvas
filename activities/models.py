from enum import unique
from django.db import models
from django.contrib.auth.models import User


class Activity(models.Model):
    title = models.CharField(max_length= 255, unique=True)
    points = models.IntegerField()


class Submission(models.Model):
    repo = models.URLField(max_length=255)
    grade = models.IntegerField(default=None, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="submissions")
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name="submissions") 
