from django.shortcuts import get_object_or_404, render
from rest_framework import status
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.serializers import Serializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

from activities.models import Activity, Submission
from activities.serializers import ActivitySerializer, SubmissionSerializer, RateSubmissionSerializer
from accounts.permissions import IsInstructor, IsFacilitator, IsStudent

import ipdb


class ActivityView(APIView): 

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor | IsFacilitator]

    def post(self, request):
        serializer = ActivitySerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        try: 
            activity, created = Activity.objects.get_or_create(**serializer.validated_data)
            serializer = ActivitySerializer(activity)

            if created:
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            return Response(serializer.data, status=status.HTTP_200_OK)
    
        except IntegrityError:
            activity = Activity.objects.get(title=serializer.validated_data['title'])
            activity.points = serializer.validated_data['points']
            activity.save()
            
            serializer = ActivitySerializer(activity)

            return Response(serializer.data, status=status.HTTP_200_OK)

    def get(self, request):
        activities = Activity.objects.all()
        serializer = ActivitySerializer(activities, many=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)


class ActivitySubmissionView(APIView): 

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsStudent]

    def post(self, request, activity_id=''):
        data = request.data
        data["activity_id"] = activity_id
        data["user_id"] = request.user.id

        serializer = SubmissionSerializer(data=data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        try:
            activity = Activity.objects.get(id=activity_id)
        except ObjectDoesNotExist:
            return Response({'errors': 'invalid activity_id'}, status=status.HTTP_404_NOT_FOUND)
        
        try:
            submission = Submission.objects.create(**serializer.validated_data)
        except IntegrityError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = SubmissionSerializer(submission)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class SubmissionDetailView(APIView): 

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor | IsFacilitator]

    def put(self, request, submission_id=''):
        serializer = RateSubmissionSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        try:
            submission = get_object_or_404(Submission, id=submission_id)
        except ObjectDoesNotExist:
            return Response({'errors': 'invalid activity_id'}, status=status.HTTP_404_NOT_FOUND)
        
        submission.grade = serializer.validated_data['grade']

        if submission.grade > submission.activity.points:
            return Response({'errors': 'invalid grade'}, status=status.HTTP_400_BAD_REQUEST)

        submission.save()

        serializer = SubmissionSerializer(submission)

        return Response(serializer.data, status=status.HTTP_200_OK)



class SubmissionView(APIView): 

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user

        if not user.is_staff and not user.is_superuser:
            submissions = Submission.objects.filter(user_id=user.id)

        else:
            submissions = Submission.objects.all()

        serializer = SubmissionSerializer(submissions, many=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)


