from enum import unique
from rest_framework import serializers


class SubmissionSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    grade = serializers.IntegerField(read_only=True)
    repo = serializers.URLField()
    user_id = serializers.IntegerField()
    activity_id = serializers.IntegerField()


class ActivitySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField()
    points = serializers.IntegerField()
    submissions = SubmissionSerializer(many=True, read_only=True)


class RateSubmissionSerializer(serializers.Serializer):
    grade = serializers.IntegerField(min_value=0)