from django.conf.urls import url
from django.urls import path
from activities.views import ActivityView, ActivitySubmissionView, SubmissionDetailView, SubmissionView

urlpatterns = [
    path('activities/', ActivityView.as_view()),
    path('activities/<int:activity_id>/submissions/', ActivitySubmissionView.as_view()),
    path('submissions/<int:submission_id>/', SubmissionDetailView.as_view()),
    path('submissions/', SubmissionView.as_view()),    
]