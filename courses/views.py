from django.shortcuts import get_object_or_404, render
from rest_framework import status
from rest_framework.serializers import Serializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

from accounts.permissions import IsInstructor, IsFacilitator, IsStudent
from courses.permissions import IsInstructorOrReadOnly
from courses.serializers import CourseSerializer, CourseRegistrationsSerializer
from courses.models import Course

import ipdb


class CourseView(APIView): 

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructorOrReadOnly]

    def post(self, request):
        serializer = CourseSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        course, created = Course.objects.get_or_create(**serializer.validated_data)
        serializer = CourseSerializer(course)
        
        if created:
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.data, status=status.HTTP_409_CONFLICT)

    def get(self, request):
        courses = Course.objects.all()
        serializer = CourseSerializer(courses, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class CourseDetailView(APIView): 

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructorOrReadOnly]       

    def get(self, request, course_id=''):
        # course = get_object_or_404(Course, id=course_id)
        try:
            course = Course.objects.get(id=course_id)
            serializer = CourseSerializer(course)

            return Response(serializer.data, status=status.HTTP_200_OK)
        
        except ObjectDoesNotExist:

            return Response({'errors': 'invalid course_id'}, status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, course_id=''):
        course = get_object_or_404(Course, id=course_id)
        course.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class CourseRegistrationsView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsInstructor]        
    
    def put(self, request, course_id=''):
        serializer = CourseRegistrationsSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        try:
            course = Course.objects.get(id=course_id)
        except ObjectDoesNotExist:
            return Response({'errors': 'invalid course_id'}, status=status.HTTP_404_NOT_FOUND)

        students_ids = serializer.validated_data['user_ids']
        
        students_list = []
        for student_id in students_ids:
            try:
                student = User.objects.get(id=student_id)
                if student.is_superuser or student.is_staff:
                    return Response(
                        {'errors': 'Only students can be enrolled in the course.'}, 
                        status=status.HTTP_400_BAD_REQUEST
                        )
                students_list.append(student)
            except ObjectDoesNotExist:
                return Response({'errors': 'invalid user_id list'}, status=status.HTTP_404_NOT_FOUND)

        course.users.set(students_list)
        serializer = CourseSerializer(course)
        
        return Response(serializer.data, status.HTTP_200_OK)

    def delete(self, request, course_id):
        course = get_object_or_404(Course, id=course_id)
        course.users.clear()

        return Response(status=status.HTTP_204_NO_CONTENT)


