from django.conf.urls import url
from django.urls import path
from courses.views import CourseView, CourseDetailView, CourseRegistrationsView

urlpatterns = [
    path('courses/', CourseView.as_view()),
    path('courses/<int:course_id>/', CourseDetailView.as_view()),
    path('courses/<int:course_id>/registrations/', CourseRegistrationsView.as_view()),
]