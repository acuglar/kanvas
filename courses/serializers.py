from rest_framework import serializers


class CourseUserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField()


class CourseSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    users = CourseUserSerializer(many=True, read_only=True)


class CourseRegistrationsSerializer(serializers.Serializer):
    user_ids = serializers.ListField(child=serializers.IntegerField())